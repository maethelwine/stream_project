# coding: utf-8

from ..stream import Stream

def test_play():
    stream = Stream()
    stream.play()
    assert stream.is_paused() == False
    stream.close()

def test_pause():
    stream = Stream()
    stream.pause()
    assert stream.is_paused() == True
    stream.close()

def test_seek():
    stream = Stream()
    stream.pause()
    stream.seek(196.210205)
    assert stream.get_time() == '196.210205'
    stream.close()