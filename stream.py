# coding: utf-8

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from webdriver_manager.chrome import ChromeDriverManager

DEFAULT_ADDRESS = "http://0.0.0.0:8000/"

class Stream():
    def __init__(self, address=DEFAULT_ADDRESS):
        service = ChromeService(executable_path=ChromeDriverManager().install())
        self.driver = webdriver.Chrome(service=service)
        self.driver.get(address)

    def load(self):
        self.driver.execute_script('document.getElementById("demoplayer").load()')

    def play(self):
        self.driver.execute_script('document.getElementById("demoplayer").play()')

    def pause(self):
        self.driver.execute_script('document.getElementById("demoplayer").pause()')
    
    def is_paused(self):
        return self.driver.execute_script('return document.getElementById("demoplayer").paused')

    def seek(self, time):
        self.driver.execute_script('document.getElementById("demoplayer").currentTime = {}'.format(time))

    def get_time(self):
        return self.driver.execute_script('return document.getElementById("demoplayer").currentTime.toString()')

    def close(self):
        self.driver.quit()
