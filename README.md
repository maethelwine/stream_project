# Step 1

Start the python server with : 

`python3 -m http.server 8000`

Screenshot : 

![Screenshot stream page!](/screenshots/step1/screenshot_stream.png)

# Step 2

Screenshots in folder screenshots/step2

Screenshot : 

![Screenshot Charles Proxy!](/screenshots/step2/screenshot_charlesproxy.png)

With Charles Proxy I've seen that first we download the stream from the video.
Next we send a request to the DNA.
What I understand is that next streamstuff get the video from the first viewer and send it to the second one from the DNA.

With Charles Proxy, I had a bug on the graphic, it seems that there is no more users if I simulate one. 

# Step 3 

With the server from step 1 launched, launch the selenium script : 

`python3 stream_process.py`

Launch the tests with pytest (https://docs.pytest.org/en/6.2.x/)


