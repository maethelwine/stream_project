# coding: utf-8

from stream import Stream

stream1 = Stream()
stream2 = Stream()
stream3 = Stream()
stream4 = Stream()
stream5 = Stream()

stream1.play()
stream2.pause()
stream3.pause()
stream3.seek(500.039623)
stream4.seek(200.039623)
stream5.seek(100.039623)
stream5.pause()

stream1.close()
stream2.close()
stream3.close()
stream4.close()
stream5.close()